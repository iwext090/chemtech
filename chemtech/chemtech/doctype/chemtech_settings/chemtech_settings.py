# -*- coding: utf-8 -*-
# Copyright (c) 2019, vincentnguyen.t090@gmail.com and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class ChemtechSettings(Document):
	pass

@frappe.whitelist()
def get_series():
	return {
		"opportunity_series" : frappe.get_meta("Opportunity").get_options("naming_series") or "OPP-WOO-",
	}	
