// Copyright (c) 2019, vincentnguyen.t090@gmail.com and contributors
// For license information, please see license.txt

frappe.ui.form.on('Chemtech Settings', {
	refresh: function(frm) {

	},
	onload: function(frm) {
		frappe.call({
			method: "chemtech.chemtech.doctype.chemtech_settings.chemtech_settings.get_series",
			callback: function (r) {
				$.each(r.message, function (key, value) {
					set_field_options(key, value);
				});
			}
		});
	}
});
