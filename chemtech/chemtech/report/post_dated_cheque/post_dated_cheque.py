# Copyright (c) 2013, vincentnguyen.t090@gmail.com and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe

def execute(filters=None):
	if not filters: filters = {}

	columns = get_columns()
	data = get_journal_invoices(filters)
	return columns, data, None, None
	
def get_columns():
	columns = []
	return [
		"Date:Date:100", "Journal Entry:Link/Journal Entry:100", "Title::200", 
		"Cheque No::200","Cheque Date:Date:100", "Total:Currency:150",
		"Remark::200" , "Doc Status::100" ,"Owner::150"
	]
	return columns

def get_journal_invoices(filters):
	conditions = get_conditions(filters)

	docstatus = """ 
		CASE
		WHEN j.docstatus = 0 THEN "Draft"
		WHEN j.docstatus = 1 THEN "Submitted"
		ELSE "Cancelled"
		END
	"""

	query = """SELECT j.posting_date, j.name, j.title, j.cheque_no, j.cheque_date, 
	j.total_amount, j.remark,
	CASE
		WHEN j.docstatus = 0 THEN "Draft"
		WHEN j.docstatus = 1 THEN "Submitted"
		ELSE "Cancelled"
	END,
	j.owner
	FROM `tabJournal Entry` j
	WHERE 1 
	%s
	""" %(conditions)

	data = frappe.db.sql(query, as_list=1)

	return data
	

def get_conditions(filters):
	conditions = ""

	docstatus = {"Draft": 0, "Submitted": 1, "Cancelled": 2}

	if filters.get("docstatus"):
		conditions += "and j.docstatus = {0}".format(docstatus[filters.get("docstatus")])
	
	if filters.get("company"):
		conditions += " and j.company = '%s'" % filters["company"]
	
	if filters.get("voucher_type"):
		conditions += " and j.voucher_type = '%s'" % filters["voucher_type"]

	if filters.get("from_date"):
		conditions += " and j.cheque_date >= '%s'" % filters["from_date"]

	if filters.get("to_date"):
		conditions += " and j.cheque_date <= '%s'" % filters["to_date"]
	
	return conditions	

