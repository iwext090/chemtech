// Copyright (c) 2016, vincentnguyen.t090@gmail.com and contributors
// For license information, please see license.txt
/* eslint-disable */

frappe.query_reports["Post Dated Cheque"] = {
	"filters": [
		{
			"fieldname": "from_date",
			"label": __("From Date"),
			"fieldtype": "Date",
			"default": frappe.datetime.get_today(),
		},
		{
			"fieldname": "to_date",
			"label": __("To Date"),
			"fieldtype": "Date",
			"default": frappe.datetime.get_today(),
		},
		{
			"fieldname": "voucher_type",
			"label": __("Entry Type"),
			"fieldtype": "Select",
			options: [
				"",
				"Journal Entry",
				"Inter Company Journal Entry",
				"Bank Entry",
				"Cash Entry",
				"Credit Card Entry",
				"Debit Note",
				"Credit Note",
				"Contra Entry",
				"Excise Entry",
				"Write Off Entry",
				"Opening Entry",
				"Depreciation Entry",
				"Exchange Rate Revaluation",
			],
			"default": "Bank Entry",
		},
		{
			"fieldname":"company",
			"label": "Company",
			"fieldtype": "Link",
			"options": "Company",
			"default": frappe.sys_defaults.company
		},
		{
			"fieldname":"docstatus",
			"label":__("Document Status"),
			"fieldtype":"Select",
			"options":["Draft", "Submitted", "Cancelled"],
			"default":"Submitted"
		}
	]
}


