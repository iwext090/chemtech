from __future__ import unicode_literals
import frappe, base64, hashlib, hmac, json, requests, time
import datetime
from frappe import _
from frappe.utils import flt, today, get_link_to_form, fmt_money
from erpnext.erpnext_integrations.connectors.woocommerce_connection import link_item, add_tax_details
from frappe.permissions import add_user_permission, remove_user_permission, \
	set_user_permission_if_allowed, has_permission

def verify_request(data):
	woocommerce_settings = frappe.get_doc("Woocommerce Settings")
	sig = base64.b64encode(
		hmac.new(
			woocommerce_settings.secret.encode('utf8'),
			data,
			hashlib.sha256
		).digest()
	)
	
	if data and \
		frappe.get_request_header("X-Wc-Webhook-Signature") and \
		not sig == bytes(frappe.get_request_header("X-Wc-Webhook-Signature").encode()):
			frappe.throw(_("Unverified Webhook Data"))
			
	frappe.set_user(woocommerce_settings.modified_by)

def order2(data=None):
	# Emulate Woocommerce Request
	headers = {
		"X-Wc-Webhook-Event":"created",
		"X-Wc-Webhook-Signature":"Ea90N+GFIzg+gDOFmZFkxZcELZGuHf1PvzCgkneq334=Vincent"
	}
	# Emulate Request Data
	data = """{\"id\":2871,\"parent_id\":0,\"number\":\"2871\",\"order_key\":\"wc_order_S2EayusmoqqsO\",\"created_via\":\"\",\"version\":\"3.6.4\",\"status\":\"ywraq-new\",\"currency\":\"QAR\",\"date_created\":\"2019-07-17T18:15:18\",\"date_created_gmt\":\"2019-07-17T18:15:18\",\"date_modified\":\"2019-07-17T18:15:18\",\"date_modified_gmt\":\"2019-07-17T18:15:18\",\"discount_total\":\"0.00\",\"discount_tax\":\"0.00\",\"shipping_total\":\"0.00\",\"shipping_tax\":\"0.00\",\"cart_tax\":\"0.00\",\"total\":\"0.00\",\"total_tax\":\"0.00\",\"prices_include_tax\":false,\"customer_id\":1,\"customer_ip_address\":\"171.249.163.121\",\"customer_user_agent\":\"Mozilla\\/5.0 (X11; Linux x86_64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/64.0.3282.167 Safari\\/537.36\",\"customer_note\":\"\",\"billing\":{\"first_name\":\"Vincent\",\"last_name\":\"Nguyen\",\"company\":\"\",\"address_1\":\"235 1A High way\",\"address_2\":\"\",\"city\":\"HCM\",\"state\":\"\",\"postcode\":\"\",\"country\":\"VN\",\"email\":\"vinhbk2000@gmail.com\",\"phone\":\"0903684987\"},\"shipping\":{\"first_name\":\"\",\"last_name\":\"\",\"company\":\"\",\"address_1\":\"\",\"address_2\":\"\",\"city\":\"\",\"state\":\"\",\"postcode\":\"\",\"country\":\"\"},\"payment_method\":\"\",\"payment_method_title\":\"\",\"transaction_id\":\"\",\"date_paid\":null,\"date_paid_gmt\":null,\"date_completed\":null,\"date_completed_gmt\":null,\"cart_hash\":\"\",\"meta_data\":[{\"id\":14557,\"key\":\"ywraq_raq\",\"value\":\"yes\"},{\"id\":14575,\"key\":\"_current_user\",\"value\":\"1\"},{\"id\":14576,\"key\":\"ywraq_raq_status\",\"value\":\"pending\"},{\"id\":14577,\"key\":\"ywraq_raq\",\"value\":\"yes\"},{\"id\":14578,\"key\":\"_ywraq_version\",\"value\":\"2.1.9\"},{\"id\":14579,\"key\":\"_ywraq_pdf_crypt\",\"value\":\"yes\"},{\"id\":14580,\"key\":\"ywraq_customer_name\",\"value\":\"Vincent Nguyen\"},{\"id\":14581,\"key\":\"ywraq_customer_message\",\"value\":\"test\"},{\"id\":14582,\"key\":\"ywraq_customer_email\",\"value\":\"vinhbk2000@gmail.com\"},{\"id\":14583,\"key\":\"_raq_request\",\"value\":{\"first_name\":{\"id\":\"first_name\",\"type\":\"text\",\"label\":\"First Name\",\"connect_to_field\":\"billing_first_name\",\"value\":\"Vincent\"},\"last_name\":{\"id\":\"last_name\",\"type\":\"text\",\"label\":\"Last Name\",\"connect_to_field\":\"billing_last_name\",\"value\":\"Nguyen\"},\"Company_Name\":{\"id\":\"Company_Name\",\"type\":\"text\",\"label\":\"Company Name\",\"connect_to_field\":\"billing_company\",\"value\":\"\"},\"Address_Line_1\":{\"id\":\"Address_Line_1\",\"type\":\"text\",\"label\":\"Address Line 1\",\"connect_to_field\":\"billing_address_1\",\"value\":\"235 1A High way\"},\"Address_Line_2\":{\"id\":\"Address_Line_2\",\"type\":\"text\",\"label\":\"Address Line 2\",\"connect_to_field\":\"billing_address_2\",\"value\":\"\"},\"City\":{\"id\":\"City\",\"type\":\"text\",\"label\":\"City\",\"connect_to_field\":\"billing_city\",\"value\":\"HCM\"},\"State\":{\"id\":\"State\",\"type\":\"text\",\"label\":\"State\",\"connect_to_field\":\"billing_state\",\"value\":\"\"},\"Country\":{\"id\":\"Country\",\"type\":\"country\",\"label\":\"Country\",\"connect_to_field\":\"billing_country\",\"value\":\"VN\"},\"user_country\":\"VN\",\"Postal_Code\":{\"id\":\"Postal_Code\",\"type\":\"text\",\"label\":\"Postal Code\",\"connect_to_field\":\"billing_postcode\",\"value\":\"\"},\"WhatsApp\":{\"id\":\"WhatsApp\",\"type\":\"tel\",\"label\":\"WhatsApp\",\"connect_to_field\":\"billing_phone\",\"value\":\"0903684987\"},\"email\":{\"id\":\"email\",\"type\":\"email\",\"label\":\"Email\",\"connect_to_field\":\"billing_email\",\"value\":\"vinhbk2000@gmail.com\"},\"message\":{\"id\":\"message\",\"type\":\"textarea\",\"label\":\"Message\",\"connect_to_field\":\"\",\"value\":\"test\"},\"customer_id\":1,\"raq_content\":{\"f442d33fa06832082290ad8544a8da27\":{\"product_id\":\"1826\",\"quantity\":\"1\"},\"ecd62de20ea67e1c2d933d311b08178a\":{\"product_id\":\"1789\",\"quantity\":\"2\"}},\"user_name\":\"Vincent Nguyen\",\"user_email\":\"vinhbk2000@gmail.com\",\"user_message\":\"test\"}},{\"id\":14584,\"key\":\"ywraq_other_email_fields\",\"value\":[]},{\"id\":14585,\"key\":\"ywraq_other_email_content\",\"value\":\"\"}],\"line_items\":[{\"id\":35,\"name\":\"Calcium Hypochlorite HTH 45kg Drum\",\"product_id\":1826,\"variation_id\":0,\"quantity\":1,\"tax_class\":\"\",\"subtotal\":\"0.00\",\"subtotal_tax\":\"0.00\",\"total\":\"0.00\",\"total_tax\":\"0.00\",\"taxes\":[],\"meta_data\":[],\"sku\":\"\",\"price\":0},{\"id\":36,\"name\":\"Chlorine Tablets TCCA 50kg Bag\",\"product_id\":1789,\"variation_id\":0,\"quantity\":2,\"tax_class\":\"\",\"subtotal\":\"0.00\",\"subtotal_tax\":\"0.00\",\"total\":\"0.00\",\"total_tax\":\"0.00\",\"taxes\":[],\"meta_data\":[],\"sku\":\"\",\"price\":0}],\"tax_lines\":[],\"shipping_lines\":[],\"fee_lines\":[],\"coupon_lines\":[],\"refunds\":[]}"""

	# sig = base64.b64encode(
	# 	hmac.new(
	# 		"d6555a686df73a900dbc6c3afad33556fad4f00cdada2eb4bc2e89c0".encode('utf8'),
	# 		data,
	# 		hashlib.sha256
	# 	).digest()
	# )
	# return sig

	url = "http://localhost:8001/api/method/erpnext.erpnext_integrations.connectors.woocommerce_connection.order"

	r = requests.post(url=url, headers=headers, data=data)
	
	time.sleep(5)
	# frappe.errprint(r)
	return r	

@frappe.whitelist(allow_guest=True)
def order(data=None):
	woocommerce_settings = frappe.get_doc("Woocommerce Settings")
	chemtech_settings = frappe.get_doc("Chemtech Settings")
	if not data:
		data = frappe.request.data
	
	verify_request(data)

	if frappe.request and frappe.request.data:
		fd = json.loads(frappe.request.data)
	elif data:
		fd = data
	else:
		return "success"

	if not data:
		event = frappe.get_request_header("X-Wc-Webhook-Event")
	else:
		event = "created"

	raw_billing_data = fd.get("billing")
	customer_woo_com_email = raw_billing_data.get("email")
	
	doc_status = fd.get("status")
	doctype = "Sales Order"

	if doc_status == "ywraq-new":
		doctype = "Opportunity"

	if event == "created":

		raw_billing_data = fd.get("billing")
		customer_woo_com_email = raw_billing_data.get("email")

		if doctype == "Opportunity":
			if frappe.get_value("Lead",{"email_id": customer_woo_com_email}):
				# Edit
				link_lead_and_address(raw_billing_data,1)
			else:
				# Create
				link_lead_and_address(raw_billing_data,0)

			lead = frappe.get_doc("Lead", {"email_id": customer_woo_com_email})
			address = frappe.get_doc("Address",{"woocommerce_email":customer_woo_com_email})

			
			items_list = fd.get("line_items")
			for item in items_list:
				item_woo_com_id = item.get("product_id")

				if frappe.get_value("Item",{"woocommerce_id": item_woo_com_id}):
					#Edit
					link_item(item,1)
				else:
					link_item(item,0)

			customer_name = raw_billing_data.get("first_name") + " " + raw_billing_data.get("last_name")

			opportunity = frappe.new_doc("Opportunity")
			opportunity.opportunity_from = "Lead"
			opportunity.party_name = lead.name
			opportunity.opportunity_type = "Sales"
			opportunity.with_items = 1
			opportunity.source = chemtech_settings.lead_source
			opportunity.customer_address = address.name

			created_date = fd.get("date_created").split("T")
			opportunity.transaction_date = created_date[0]

			opportunity.woocommerce_id = fd.get("id")
			# opportunity.to_discuss = "WooCommerce id: {0}".format(fd.get("id"))
			opportunity.naming_series = chemtech_settings.opportunity_series or "OPP-WOO-"	

			default_set_company = frappe.get_doc("Global Defaults")
			#company = raw_billing_data.get("company") or default_set_company.default_company
			company = default_set_company.default_company
			found_company = frappe.get_doc("Company",{"name":company})
			company_abbr = found_company.abbr

			opportunity.company = company 

			for item in items_list:
				woocomm_item_id = item.get("product_id")
				found_item = frappe.get_doc("Item",{"woocommerce_id": woocomm_item_id})

				opportunity.append("items",{
					"item_code": found_item.item_code,
					"item_name": found_item.item_name,
					"description": found_item.item_name,
					"uom": woocommerce_settings.uom or _("Nos"),
					"qty": item.get("quantity"),
					})
				
			opportunity.save()
			frappe.db.commit()

		if doctype == "Sales Order":
			if frappe.get_value("Customer",{"woocommerce_email": customer_woo_com_email}):
				# Edit
				link_customer_and_address(raw_billing_data,1)
			else:
				# Create
				link_customer_and_address(raw_billing_data,0)


			items_list = fd.get("line_items")
			for item in items_list:

				item_woo_com_id = item.get("product_id")

				if frappe.get_value("Item",{"woocommerce_id": item_woo_com_id}):
					#Edit
					link_item(item,1)
				else:
					link_item(item,0)


			customer_name = raw_billing_data.get("first_name") + " " + raw_billing_data.get("last_name")

			new_sales_order = frappe.new_doc("Sales Order")
			new_sales_order.customer = customer_name

			created_date = fd.get("date_created").split("T")
			new_sales_order.transaction_date = created_date[0]

			new_sales_order.po_no = fd.get("id")
			new_sales_order.woocommerce_id = fd.get("id")
			new_sales_order.naming_series = woocommerce_settings.sales_order_series or "SO-WOO-"

			placed_order_date = created_date[0]
			raw_date = datetime.datetime.strptime(placed_order_date, "%Y-%m-%d")
			raw_delivery_date = frappe.utils.add_to_date(raw_date,days = 7)
			order_delivery_date_str = raw_delivery_date.strftime('%Y-%m-%d')
			order_delivery_date = str(order_delivery_date_str)

			new_sales_order.delivery_date = order_delivery_date
			default_set_company = frappe.get_doc("Global Defaults")
			#company = raw_billing_data.get("company") or default_set_company.default_company
			company = default_set_company.default_company
			found_company = frappe.get_doc("Company",{"name":company})
			company_abbr = found_company.abbr

			new_sales_order.company = company

			for item in items_list:
				woocomm_item_id = item.get("product_id")
				found_item = frappe.get_doc("Item",{"woocommerce_id": woocomm_item_id})

				ordered_items_tax = item.get("total_tax")

				new_sales_order.append("items",{
					"item_code": found_item.item_code,
					"item_name": found_item.item_name,
					"description": found_item.item_name,
					"delivery_date":order_delivery_date,
					"uom": woocommerce_settings.uom or _("Nos"),
					"qty": item.get("quantity"),
					"rate": item.get("price"),
					"warehouse": woocommerce_settings.warehouse or "Stores" + " - " + company_abbr
					})

				if flt(ordered_items_tax) > 0:
					add_tax_details(new_sales_order,ordered_items_tax,"Ordered Item tax",0)

			# shipping_details = fd.get("shipping_lines") # used for detailed order
			shipping_total = fd.get("shipping_total")
			shipping_tax = fd.get("shipping_tax")

			if flt(shipping_tax) > 0:
				add_tax_details(new_sales_order,shipping_tax,"Shipping Tax",1)
			
			if flt(shipping_total) > 0:
				add_tax_details(new_sales_order,shipping_total,"Shipping Total",1)

			new_sales_order.submit()

			frappe.db.commit()

def link_customer_and_address(raw_billing_data,customer_status):
	woocommerce_settings = frappe.get_doc("Woocommerce Settings")
	chemtech_settings = frappe.get_doc("Chemtech Settings")
	default_set_company = frappe.get_doc("Global Defaults")
	
	if customer_status == 0:
		# create
		customer = frappe.new_doc("Customer")
		address = frappe.new_doc("Address")
		
		company = default_set_company.default_company		
		customer.customer_of = woocommerce_settings.creation_user
		customer.payment_terms = chemtech_settings.payment_terms
		customer.append("accounts", {
			"company": company,
			"account" : chemtech_settings.default_receivable_accounts
		})

	if customer_status == 1:
		# Edit
		customer_woo_com_email = raw_billing_data.get("email")
		customer = frappe.get_doc("Customer",{"woocommerce_email": customer_woo_com_email})
		old_name = customer.customer_name

	full_name = str(raw_billing_data.get("first_name"))+ " "+str(raw_billing_data.get("last_name"))
	customer.customer_name = full_name
	customer.woocommerce_email = str(raw_billing_data.get("email"))

	customer.save()
	frappe.db.commit()

	if customer_status == 1:
		if old_name != full_name:
			frappe.rename_doc("Customer", old_name, full_name)
		address = frappe.get_doc("Address",{"woocommerce_email":customer_woo_com_email})
		customer = frappe.get_doc("Customer",{"woocommerce_email": customer_woo_com_email})

	address.address_line1 = raw_billing_data.get("address_1", "Not Provided")
	address.address_line2 = raw_billing_data.get("address_2", "Not Provided")
	address.city = raw_billing_data.get("city", "Not Provided")
	address.woocommerce_email = str(raw_billing_data.get("email"))
	address.address_type = "Shipping"
	address.country = frappe.get_value("Country", filters={"code":raw_billing_data.get("country", "IN").lower()})
	address.state =  raw_billing_data.get("state")
	address.pincode =  str(raw_billing_data.get("postcode"))
	address.phone = str(raw_billing_data.get("phone"))
	address.email_id = str(raw_billing_data.get("email"))

	address.append("links", {
		"link_doctype": "Customer",
		"link_name": customer.customer_name
	})

	address.save()
	frappe.db.commit()

	if customer_status == 1:

		address = frappe.get_doc("Address",{"woocommerce_email":customer_woo_com_email})
		old_address_title = address.name
		new_address_title = customer.customer_name+"-billing"
	
		address.address_title = customer.customer_name
		address.save()

		if old_address_title != new_address_title:
			frappe.rename_doc("Address",old_address_title,new_address_title)

	frappe.db.commit()


def link_lead_and_address(raw_billing_data, lead_status):
	woocommerce_settings = frappe.get_doc("Woocommerce Settings")
	chemtech_settings = frappe.get_doc("Chemtech Settings")
	default_set_company = frappe.get_doc("Global Defaults")
	
	if lead_status == 0:
		# create
		lead = frappe.new_doc("Lead")
		address = frappe.new_doc("Address")
		
		company = default_set_company.default_company		
		lead.lead_owner = woocommerce_settings.creation_user

	if lead_status == 1:
		# Edit
		customer_woo_com_email = raw_billing_data.get("email")
		email_id = raw_billing_data.get("email")
		lead = frappe.get_doc("Lead",{"email_id": email_id})
		old_name = lead.lead_name

	full_name = str(raw_billing_data.get("first_name"))+ " "+str(raw_billing_data.get("last_name"))
	lead.lead_name = full_name
	lead.email_id = str(raw_billing_data.get("email"))
	lead.mobile_no = str(raw_billing_data.get("phone"))
	lead.source = chemtech_settings.lead_source
	lead.salutation = "Mr"

	lead.save()
	frappe.db.commit()

	if lead_status == 1:
		if old_name != full_name:
			frappe.rename_doc("Lead", old_name, full_name)
		address = frappe.get_doc("Address",{"woocommerce_email":customer_woo_com_email})
		lead = frappe.get_doc("Lead",{"email_id": email_id})

	address.address_line1 = raw_billing_data.get("address_1", "Not Provided")
	address.address_line2 = raw_billing_data.get("address_2", "Not Provided")
	address.city = raw_billing_data.get("city", "Not Provided")
	address.woocommerce_email = str(raw_billing_data.get("email"))
	address.address_type = "Shipping"
	address.country = frappe.get_value("Country", filters={"code":raw_billing_data.get("country", "IN").lower()})
	address.state =  raw_billing_data.get("state")
	address.pincode =  str(raw_billing_data.get("postcode"))
	address.phone = str(raw_billing_data.get("phone"))
	address.email_id = str(raw_billing_data.get("email"))

	address.append("links", {
		"link_doctype": "Lead",
		"link_name": lead.name
	})

	address.save()
	frappe.db.commit()

	# if lead_status == 1:

	# 	address = frappe.get_doc("Address",{"woocommerce_email":email_id})
	# 	old_address_title = address.name
	# 	new_address_title = lead.lead_name+"-billing"
	
	# 	address.address_title = lead.lead_name
	# 	address.save()

	# 	if old_address_title != new_address_title:
	# 		frappe.rename_doc("Address",old_address_title,new_address_title)

	# frappe.db.commit()	


def customer_on_update(doc, method):

	if not has_permission('User Permission', ptype='write', raise_exception=False): return

	if not doc.create_user_permission or not doc.customer_of: return

	user = frappe.get_doc("User", doc.customer_of)

	if user.role_profile_name != "Sales User": return

	customer_user_permission_exists = frappe.db.exists('User Permission', {
		'allow': 'Customer',
		'for_value': doc.name,
		'user': doc.customer_of
	})

	if customer_user_permission_exists: return

	add_user_permission("Customer", doc.name, doc.customer_of)


@frappe.whitelist()
def patch_set_customer_user_permission():
	sql = """ 
		SELECT c.name, customer_of 
		FROM `tabCustomer` c
		LEFT JOIN `tabUser` u ON u.name = c.customer_of
		WHERE 
			c.customer_of IS NOT NULL
        	AND u.role_profile_name = 'Sales User'
	"""
	for customer, user in frappe.db.sql(sql):
		frappe.permissions.add_user_permission("Customer", customer, user)
	
	frappe.db.commit()


@frappe.whitelist()
def reminder_post_dated_cheque():
	alert_post_dated_cheque = frappe.db.get_single_value("Chemtech Settings", "alert_post_dated_cheque") or 0

	cheque_date = today()
	voucher_type = "Bank Entry"
	
	if not alert_post_dated_cheque:
		return 0

	subject = "Reminder Post Dated Cheque list is submitted on {cheque_date}"
		
	message = """
		Hi,<br>
		Your post dated cheque list is submitted on {cheque_date}. <br><br>
		{list}<br>
	"""

	query = """ 
		SELECT j.posting_date, j.name, j.title, j.cheque_no, j.cheque_date, 
		j.total_amount, j.remark, j.owner, j.total_amount_currency
		FROM `tabJournal Entry` j
		WHERE j.docstatus = 1 and voucher_type = %(voucher_type)s and j.cheque_date = %(cheque_date)s
	"""

	data = frappe.db.sql(query, {"voucher_type" : voucher_type, "cheque_date": cheque_date}, as_dict=1, debug=0)

	emails = frappe._dict()
	

	for d in data :
		if not emails.get(d.owner):
			emails[d.owner] = []

		item = """ Journal Entry: {} <br>
					-- Title: {} <br> 
					-- Cheque No: {} <br>
					-- Amount: {} <br>
					-- Remark: {} <br>
					 """.format(
							get_link_to_form("Journal Entry", d.name, label=d.name),
							d.title,
							d.cheque_no,
							fmt_money(d.total_amount, 2, d.total_amount_currency),
							d.remark,
						)

		emails[d.owner].append(item)

	for email, lists in emails.items():

		frappe.sendmail(
			recipients=email,
			subject= subject.format(cheque_date = cheque_date),
			message=message.format(cheque_date = cheque_date, list = '<br>'.join(lists))
		)		

	return len(data)