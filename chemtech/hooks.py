# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from . import __version__ as app_version

app_name = "chemtech"
app_title = "Chemtech"
app_publisher = "vincentnguyen.t090@gmail.com"
app_description = "Chemtech"
app_icon = "octicon octicon-file-directory"
app_color = "grey"
app_email = "vincentnguyen.t090@gmail.com"
app_license = "MIT"

# Includes in <head>
# ------------------

# include js, css files in header of desk.html
# app_include_css = "/assets/chemtech/css/chemtech.css"
# app_include_js = "/assets/chemtech/js/chemtech.js"

# include js, css files in header of web template
# web_include_css = "/assets/chemtech/css/chemtech.css"
# web_include_js = "/assets/chemtech/js/chemtech.js"

# include js in page
# page_js = {"page" : "public/js/file.js"}

# include js in doctype views
# doctype_js = {"doctype" : "public/js/doctype.js"}
# doctype_list_js = {"doctype" : "public/js/doctype_list.js"}
# doctype_tree_js = {"doctype" : "public/js/doctype_tree.js"}
# doctype_calendar_js = {"doctype" : "public/js/doctype_calendar.js"}

# Home Pages
# ----------

# application home page (will override Website Settings)
# home_page = "login"

# website user home page (by Role)
# role_home_page = {
#	"Role": "home_page"
# }

# Website user home page (by function)
# get_website_user_home_page = "chemtech.utils.get_home_page"

# Generators
# ----------

# automatically create page for each record of this doctype
# website_generators = ["Web Page"]

# Installation
# ------------

# before_install = "chemtech.install.before_install"
# after_install = "chemtech.install.after_install"

# Desk Notifications
# ------------------
# See frappe.core.notifications.get_notification_config

# notification_config = "chemtech.notifications.get_notification_config"

# Permissions
# -----------
# Permissions evaluated in scripted ways

# permission_query_conditions = {
# 	"Event": "frappe.desk.doctype.event.event.get_permission_query_conditions",
# }
#
# has_permission = {
# 	"Event": "frappe.desk.doctype.event.event.has_permission",
# }

# Document Events
# ---------------
# Hook on document methods and events

# doc_events = {
# 	"*": {
# 		"on_update": "method",
# 		"on_cancel": "method",
# 		"on_trash": "method"
#	}
# }
doc_events = {
	"Customer": {
		"on_update": "chemtech.api.customer_on_update",
	}
}

# Scheduled Tasks
# ---------------

# scheduler_events = {
# 	"all": [
# 		"chemtech.tasks.all"
# 	],
# 	"daily": [
# 		"chemtech.tasks.daily"
# 	],
# 	"hourly": [
# 		"chemtech.tasks.hourly"
# 	],
# 	"weekly": [
# 		"chemtech.tasks.weekly"
# 	]
# 	"monthly": [
# 		"chemtech.tasks.monthly"
# 	]
# }

scheduler_events = {
	"cron": {
        "09 00 * * *": [
            "chemtech.api.reminder_post_dated_cheque"
        ]
	}	
}

# Testing
# -------

# before_tests = "chemtech.install.before_tests"

# Overriding Whitelisted Methods
# ------------------------------
#
# override_whitelisted_methods = {
# 	"frappe.desk.doctype.event.event.get_events": "chemtech.event.get_events"
# }

override_whitelisted_methods = {
 	"erpnext.erpnext_integrations.connectors.woocommerce_connection.order": "chemtech.api.order"
}


