# Copyright (c) 2015, Frappe Technologies Pvt. Ltd. and Contributors
# License: GNU General Public License v3. See license.txt

from __future__ import unicode_literals
import frappe
import frappe.permissions

def execute():
	sql = """ 
		SELECT c.name, customer_of 
		FROM `tabCustomer` c
		LEFT JOIN `tabUser` u ON u.name = c.customer_of
		WHERE 
			c.customer_of IS NOT NULL
        	AND u.role_profile_name = 'Sales User'
	"""
	for customer, user in frappe.db.sql(sql):
		frappe.permissions.add_user_permission("Customer", customer, user) 
